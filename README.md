# Data Storage

The data warehouse is designed to serialize, deserealize and store information about people in .xml, .json, .binary, .yaml, .csv extensions in files.

## Requirements

JDK, IDE for Java.

## Libraries

Jackson for xml, yaml. BSON, GSON with all dependencies.

## Instructions

Select extension(.xml, .binary, .csv, .yaml, .json.) to work with file.
Select command:
"create" to create one person. User enter first name, last name, age and city. ID generate and assigned automatically;
"read" to read one person by ID;
"update" to update one person by ID. Person is searched for by ID and then the user enters updated data (first name, last name, age and city) for the person;
"readAll" to read all person;
"switch" to change file to working;
"help" to read the help;
"exit" to exit the application.

## Example

![alt text](screenshots/p1.png "Example")

![alt text](screenshots/p2.png "Example")

## Acknowledgments

Our nervous systems
Google
Stack Overflow
Coffee and energetics
Other students <3
Our teachers <3

## Designed by

Kristina Luchynchyn
Vladyslav Marchuk
Evgeny Novikov
Sergey Kovaliov

## License

Copyright © 2021 Perfect Team <3
