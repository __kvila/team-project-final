//package test.java.com.datastorage.service;
//
//import main.java.com.datastorage.service.ValidationService;
//import org.junit.jupiter.api.Assertions;
//import org.junit.jupiter.params.ParameterizedTest;
//import org.junit.jupiter.params.provider.Arguments;
//import org.junit.jupiter.params.provider.MethodSource;
//import org.mockito.Mockito;
//
//import java.util.Scanner;
//
//public class ValidationServiceTest {
//
//    private static Scanner scanner = Mockito.mock(Scanner.class);
//
//    private static ValidationService cut = new ValidationService(scanner);
//
//    static Arguments[] validateStringTestArgs() {
//        return new Arguments[]{
//                Arguments.arguments("Gayorgiy", "Gayorgiy"),
//                Arguments.arguments("Java", "    Java     "),
//                Arguments.arguments("Anna Maria", "Anna Maria")
//        };
//    }
//
//    @ParameterizedTest
//    @MethodSource("validateStringTestArgs")
//    void validateStringTest(String expected, String userAnswer) {
//        Mockito.when(scanner.next()).thenReturn(userAnswer);
//        String actual = cut.validateString();
//        Assertions.assertEquals(expected, actual);
//    }
//
//    static Arguments[] validateString1TestArgs() {
//        return new Arguments[]{
//                Arguments.arguments("Java", "Uno~", "D()s", "Tre$", "Cuatro!", "5yf", "Java"),
//                Arguments.arguments("Java", "J@va", "#ava", "J%ava", "Ja^a", "Java&", "Java"),
//                Arguments.arguments("Jean-Mark", "Kira*", "K_i", "Kim++", "Y=ra", "stop?", "Jean-Mark"),
//                Arguments.arguments("one", ">ra", "<money", "o/ne", "o\"", "U,d", "one"),
//                Arguments.arguments("one", "oe\n", "t;o", "thr:e", "f{r", "h}", "one"),
//        };
//    }
//
//    @ParameterizedTest
//    @MethodSource("validateString1TestArgs")
//    void validateString1Test(String expected, String userAnswer,
//                             String userAnswer2, String userAnswer3,
//                             String userAnswer4, String userAnswer5,
//                             String userAnswer6) {
//        Mockito.when(scanner.next()).thenReturn(userAnswer, userAnswer2, userAnswer3, userAnswer4, userAnswer5, userAnswer6);
//        String actual = cut.validateString();
//        Assertions.assertEquals(expected, actual);
//    }
//
//    static Arguments[] validateNumberTestArgs() {
//        return new Arguments[]{
//                Arguments.arguments(1, "1"),
//                Arguments.arguments(80, "80"),
//        };
//    }
//
//    @ParameterizedTest
//    @MethodSource("validateNumberTestArgs")
//    void validateNumberTest(int expected, String userAnswer) {
//        Mockito.when(scanner.next()).thenReturn(userAnswer);
//        int actual = cut.validateNumber();
//        Assertions.assertEquals(expected, actual);
//    }
//
//    static Arguments[] validateNumber1TestArgs() {
//        return new Arguments[]{
//                Arguments.arguments(1, "one", "1/2", "1"),
//                Arguments.arguments(2, "\n7", "9.0", "2"),
//                Arguments.arguments(3, "3+1", "6 0", "3")
//        };
//    }
//
//    @ParameterizedTest
//    @MethodSource("validateNumber1TestArgs")
//    void validateNumber1Test(int expected, String userAnswer,
//                             String userAnswer2, String userAnswer3) {
//        Mockito.when(scanner.next()).thenReturn(userAnswer, userAnswer2, userAnswer3);
//        int actual = cut.validateNumber();
//        Assertions.assertEquals(expected, actual);
//    }
//}