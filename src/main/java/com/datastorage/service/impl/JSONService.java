package main.java.com.datastorage.service.impl;

import com.google.gson.Gson;
import main.java.com.datastorage.models.Person;
import main.java.com.datastorage.service.IFilesFactory;
import main.java.com.datastorage.service.ValidationService;
import main.java.com.datastorage.utils.CreatePerson;
import main.java.com.datastorage.wrapper.WrapperFileJSON;
import netscape.javascript.JSObject;


import java.io.*;
import java.util.Scanner;

public class JSONService implements IFilesFactory {


    private static final String MSG_PERSON_NOT_FOUND = "Person not found";
    private static final String MSG_PERSON_UPDATE = "Person update";
    private static final String MSG_FILE_UPDATE = "File update";
    private static final String MSG_PERSON_DELETED = "Person deleted";


    private static final String PATH = "src/personJSON.json";
    private static final String PATH2 = "src/jsonTmp.json";


    private Gson gson;
    private CreatePerson createPerson;
    private Scanner scanner;
    private ValidationService validationService;
    private WrapperFileJSON wrapperFileJSON;


    private File jsonFile;

    public JSONService(Gson gson, CreatePerson createPerson, Scanner scanner, ValidationService validationService, WrapperFileJSON wrapperFileJSON, File jsonFile) {
        this.gson = gson;
        this.createPerson = createPerson;
        this.scanner = scanner;
        this.validationService = validationService;
        this.wrapperFileJSON = wrapperFileJSON;
        this.jsonFile = jsonFile;
    }

    @Override
    public void create(){
        System.out.println("JSON");
//        final String dir = System.getProperty("user.dir");
//        System.out.println("current dir = " + dir);
        Person person = createPerson.createPerson();
        String json = gson.toJson(person);
        try (BufferedWriter bufferedWriter = wrapperFileJSON.getBufferWriterTrue(PATH)) {
            bufferedWriter.write(json+"\n");
            System.out.println("Person save");
            bufferedWriter.flush();
        } catch (IOException e) {
            System.out.println("my error js");
            e.printStackTrace();
        }
    }

    @Override
    public void update(){
        //System.out.println(MSG_ENTER_ID);
        int id = validationService.validateID();
        int findID = 0;
        File jsonFile2 = new File(PATH2); // создаю новый файл, в который буду перезаписывать
        try(BufferedReader br = new BufferedReader(new FileReader(PATH)))
        {//чтение построчно
            String s; //беру строку
            while((s=br.readLine())!=null){ //если она не пустая, продолжаю
                if(!s.contains("\"id\":"+id)){ //пока моя строка не имеет указанного ИД
                    rewrite(s, PATH2);//записываю содержимое моего файла(jsonFile) во второй файл(jsonFile2)
                }
                else {
                    findID = 1;
                    Person updatePerson = createPerson.updatePerson(id);
                    String updateJSON = gson.toJson(updatePerson);//превращаю объект в джейсон-строку
                    rewrite(updateJSON, PATH2);//добавляю джейсон строку в файл
                }
            }
            if (findID==0){
                System.out.println(MSG_PERSON_NOT_FOUND);
            }
            else System.out.println(MSG_PERSON_UPDATE);
        }
        catch(IOException e){
            System.out.println("my error js");
            e.printStackTrace();
        }
        jsonFile.delete();
        boolean success = jsonFile2.renameTo(jsonFile);

        if (success){
            System.out.println(MSG_FILE_UPDATE);
        }
        else
        {
            System.out.println("Error file");
        }


    }
    private void rewrite(String json, String pathFileToWrite){
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(pathFileToWrite, true))) {//создаю буфер райтер
            //вконце обязательно true - чтобы оно дозаписывало в конец, а не удаляло уже имеющееся
            bufferedWriter.write(json+"\n");//добавляю джейсон строку и перенос строки
            bufferedWriter.flush();//сохраняю
        } catch (IOException e) {//обязательная фигня
            System.out.println("my error js");
            e.printStackTrace();
        }
    }

    @Override
    public void delete(){
        //System.out.println();
        int id = validationService.validateID();
        int findID = 0;
        File jsonFile2 = new File(PATH2);
        try(BufferedReader br = new BufferedReader(new FileReader(PATH)))
        {//чтение построчно
            String s;
            while((s=br.readLine())!=null){
                if(!s.contains("\"id\":"+id)){
                    rewrite(s, PATH2);
                } else {
                    System.out.println("Person delete");
                }
            }
            if (findID==0){
                System.out.println(MSG_PERSON_NOT_FOUND);
            }
            else System.out.println(MSG_PERSON_DELETED);
        }
        catch(IOException e){
            System.out.println("my error js");
            e.printStackTrace();
        }
        jsonFile.delete();
        boolean success = jsonFile2.renameTo(jsonFile);

        if (success){
            System.out.println(MSG_FILE_UPDATE);
        }
        else
        {
            System.out.println("Error file");
        }

    }

    @Override
    public void read(){
        int id = validationService.validateID();
        try(BufferedReader br = new BufferedReader(new FileReader(PATH)))
        {//чтение построчно
            String s;
            while((s=br.readLine())!=null){

                if(s.contains("\"id\":"+id)){
                    Person read = gson.fromJson(s, Person.class);
                    System.out.println(read.toString());
                    return;
                }
            }
            System.out.println("ID not found");
            return;
        }
        catch(IOException e){
            System.out.println("my error js");
            e.printStackTrace();
        }
    }

    @Override
    public void readAll(){
        try(BufferedReader br = new BufferedReader(new FileReader(PATH)))
        {//чтение построчно
            String s;
            while((s=br.readLine())!=null){
                Person read = gson.fromJson(s, Person.class);
                System.out.println(read.toString());
            }
        }
        catch(IOException e){
            System.out.println("File is empty.");
            //e.printStackTrace();
        }
    }


}