package main.java.com.datastorage.service.impl;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import main.java.com.datastorage.models.Person;
import main.java.com.datastorage.service.IFilesFactory;
import main.java.com.datastorage.service.ValidationService;
import main.java.com.datastorage.utils.CreatePerson;


import java.io.*;
import java.util.Objects;
import java.util.Scanner;

public class XMLService implements IFilesFactory {

    private Scanner scanner;
    private ValidationService validationService;
    private CreatePerson createPerson;
    XmlMapper xmlMapper;

    private static final String MSG_PERSON_NOT_FOUND = "Person not found";
    private static final String MSG_PERSON_UPDATE = "Person update";
    private static final String MSG_FILE_UPDATE = "File update";
    private static final String MSG_PERSON_DELETED = "Person deleted";


    private static final String PATH = "src/xmlFile.xml";
    private static final String PATH2 = "src/xmlFile2.xml";
    private File xmlFile;

    private static final String HEAD_XML = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>";
    private static final String TAG_OPEN = "<Persons>";
    private static final String TAG_CLOSE = "</Persons>";

    public XMLService(Scanner scanner, ValidationService validationService, CreatePerson createPerson, XmlMapper xmlMapper, File xmlFile) {
        this.scanner = scanner;
        this.validationService = validationService;
        this.createPerson = createPerson;
        this.xmlMapper = xmlMapper;
        this.xmlFile = xmlFile;
    }

    @Override
    public void create() {

        File xmlFile2 = new File(PATH2); // создаю новый файл, в который буду перезаписывать
        try (BufferedReader br = new BufferedReader(new FileReader(PATH))) {//чтение построчно
            checkHeadLine(PATH);
            String s;
            s = br.readLine();//kostyl
            s = br.readLine();//kostyli
            while (!Objects.equals(s = br.readLine(), TAG_CLOSE) && (s) != null) { //если она не пустая, продолжаю
                rewrite(s, PATH2);//записываю содержимое моего файла(xmlile) во второй файл(xmlFile2)
            }
            String xmlString = xmlMapper.writeValueAsString(createPerson.createPerson());
            rewrite(xmlString, PATH2);//добавляю джейсон строку в файл
            addTagClose();
        } catch (IOException e) {
            System.out.println("my error js");
            e.printStackTrace();
        }
        xmlFile.delete();
        boolean success =
                xmlFile2.renameTo(xmlFile);
        if (success) {
            System.out.println("Person create");
        } else {
            System.out.println("Error file");
        }
    }

    @Override
    public void update() {
        int id = validationService.validateID(); // ИД со сканера ( после валидации)
        int findID = 0;
        File xmlFile2 = new File(PATH2);
        try (BufferedReader br = new BufferedReader(new FileReader(PATH))) {
            String s;
            s = br.readLine();//пропускаем сервисную строку
            s = br.readLine();//пропускаем открывающий тег
            while (!Objects.equals(s = br.readLine(), TAG_CLOSE) && (s) != null) {
                if (!s.contains("<id>" + id + "</id>")) {
                    rewrite(s, PATH2);//записываю содержимое моего файла(xmlFile) во второй файл(xmlFile2)
                } else {
                    findID = 1;
                    Person updatePerson = createPerson.updatePerson(id);
                    String updateJSON = xmlMapper.writeValueAsString(updatePerson);//превращаю объект в строку
                    rewrite(updateJSON, PATH2);//добавляю строку в файл
                }
            }
            if (findID==0){
                System.out.println(MSG_PERSON_NOT_FOUND);
            }
            else System.out.println(MSG_PERSON_UPDATE);

            addTagClose();// добавляю закрывающейся тег
        } catch (IOException e) {
            System.out.println("my error js");
            e.printStackTrace();
        }
        xmlFile.delete();
        boolean success = xmlFile2.renameTo(xmlFile);

        if (success) {
            System.out.println(MSG_FILE_UPDATE);
        } else {
            System.out.println("Error file");
        }
    }

    @Override
    public void readAll() {
        try (BufferedReader br = new BufferedReader(new FileReader(PATH))) {//чтение построчно
            String s;
            s = br.readLine();
            s = br.readLine();
            while (!Objects.equals(s = br.readLine(), TAG_CLOSE) && (s) != null) {
                Person read = xmlMapper.readValue(s, Person.class);
                System.out.println(read.toString());
            }
        } catch (IOException e) {
            System.out.println("my error js read");
            e.printStackTrace();
        }
    }

    @Override
    public void read() {
        int id = validationService.validateID();
        try (BufferedReader br = new BufferedReader(new FileReader(PATH))) {//чтение построчно
            String s;
            s = br.readLine();
            s = br.readLine();
            while

            (!Objects.equals(s = br.readLine(), TAG_CLOSE) && (s) != null) {
                if (s.contains("<id>" + id + "</id>")) {
                    Person read = xmlMapper.readValue(s, Person.class);
                    System.out.println(read.toString());
                    return;
                }
            }
            System.out.println("ID not found");
        } catch (IOException e) {
            System.out.println("my error js");
            e.printStackTrace();
        }
    }


    private void rewrite(String xmlStr, String pathFileToWrite) {
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(pathFileToWrite, true))) {
            checkHeadLine(PATH2);
            bufferedWriter.write(xmlStr + "\n");
            bufferedWriter.flush();
        } catch (IOException e) {
            System.out.println("my error js");
            e.printStackTrace();
        }
    }

    @Override
    public void delete() {
        int id = validationService.validateID();
        int findID = 0;
        File xmlFile2 = new File(PATH2);
        try (BufferedReader br = new BufferedReader(new FileReader(PATH))) {//чтение построчно
            String s;
            s = br.readLine();//serv line
            s = br.readLine();//tag
            while ((!Objects.equals(s = br.readLine(), TAG_CLOSE) && (s) != null)) {
                if (!s.contains("<id>" + id + "</id>")) {
                    rewrite(s, PATH2);
                }else {
                    findID = 1;
                }
            }
            if (findID==0){
                System.out.println(MSG_PERSON_NOT_FOUND);
            }
            else System.out.println(MSG_PERSON_DELETED);

            addTagClose();// добавляю закрывающейся тег

        } catch (IOException e) {
            System.out.println("my error js");
            e.printStackTrace();
        }
        xmlFile.delete();
        boolean success = xmlFile2.renameTo(xmlFile);

        if (success) {
            System.out.println(MSG_FILE_UPDATE);
        } else {
            System.out.println("Error file");
        }

    }


    private void addTagClose() {
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(PATH2, true))) {
            bufferedWriter.write(TAG_CLOSE + "\n");
            bufferedWriter.flush();
        } catch (IOException e) {
            System.out.println("my error js");
            e.printStackTrace();
        }
    }


    private void checkHeadLine(String nameFile) {
        try (BufferedReader br = new BufferedReader(new FileReader(nameFile))) {//чтение построчно
            String firstLine;
            String secondLine;
            firstLine = br.readLine();
            secondLine = br.readLine();
            if (firstLine == null && secondLine == null) {
                createHeadLine(nameFile);
            }
        } catch (IOException e) {
            System.out.println("my error js read");
            e.printStackTrace();
        }
    }


    private void createHeadLine(String nameFile) {
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(nameFile, true))) {
            bufferedWriter.write(HEAD_XML + "\n");
            bufferedWriter.write(TAG_OPEN + "\n");
            bufferedWriter.flush();
        } catch (IOException e) {
            System.out.println("my error js");
            e.printStackTrace();
        }
    }

    private void checkNewFile() {
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(PATH, true))) {
            checkHeadLine(PATH);
        } catch (IOException e) {
            System.out.println("my error js");
            e.printStackTrace();
        }
    }

}


