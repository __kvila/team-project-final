package main.java.com.datastorage.service.impl;

import main.java.com.datastorage.models.Person;
import main.java.com.datastorage.service.IFilesFactory;
import main.java.com.datastorage.service.ValidationService;
import main.java.com.datastorage.utils.CreatePerson;

import java.io.*;
import java.util.Scanner;

public class CSVService implements IFilesFactory {


    private CreatePerson createPerson;
    private Scanner scanner;
    private ValidationService validationService;

    private static final String MSG_PERSON_NOT_FOUND = "Person not found";
    private static final String MSG_PERSON_UPDATE = "Person update";
    private static final String MSG_FILE_UPDATE = "File update";

    private static final String PATH = "src/csvFile.csv";
    private static final String PATH2 = "src/csvFile2.csv";
    private static final String HEAD_CSV = "id,fname,lname,age,city";
    private File csvFile;

    public CSVService(CreatePerson createPerson, Scanner scanner, ValidationService validationService, File csvFile) {
        this.createPerson = createPerson;
        this.scanner = scanner;
        this.validationService = validationService;
        this.csvFile = csvFile;
    }

    @Override
    public void create() {

        Person person = createPerson.createPerson(); // �������� �����  �������� �������
        String csvString = parseToStringCsv(person);
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(PATH, true))) {
            checkHeadLine(PATH);
            bufferedWriter.write(csvString + "\n");
            System.out.println("Person save");
            bufferedWriter.flush();
        } catch (IOException e) {
            System.out.println("my error js");
            e.printStackTrace();
        }
    }


    @Override
    public void readAll() {

        try (BufferedReader br = new BufferedReader(new FileReader(PATH))) {//������ ���������
            String s;
            String showLine;
            String[] csvArr;
            s = br.readLine();
            while ((s = br.readLine()) != null) {
                csvArr = s.split(",");
                showLine = "ID - " + csvArr[0] + ", Fname - " + csvArr[1] + ", Lname - "+ csvArr[2] + ", age - " +  csvArr[3] + ", city - " +  csvArr[4];
                System.out.println(showLine);
            }
        } catch (IOException e) {
            System.out.println("File is empty.");
            //e.printStackTrace();
        }
    }

    @Override
    public void read() {
        int id = validationService.validateID();
        String showLine;
        String[] csvArr;
        try (BufferedReader br = new BufferedReader(new FileReader(PATH))) {//������ ��������� 
            String s;
            s = br.readLine();
            while ((s = br.readLine()) != null) {
                csvArr = s.split(",");
                if ( Integer.parseInt(csvArr[0]) == id ) {
                    showLine = "ID - " + csvArr[0] + ", Fname - " + csvArr[1] + ", Lname - "+ csvArr[2] + ", age - " +  csvArr[3] + ", city - " +  csvArr[4];
                    System.out.println(showLine);
                    return;
                }
            }
            System.out.println("ID not found");
        } catch (IOException e) {
            System.out.println("my error js");
            e.printStackTrace();
        }
    }

    @Override
    public void update(){
        String[] csvArr;
        int id = validationService.validateID();
        int findID = 0;
        File csvFile22 = new File(PATH2);
        try(BufferedReader br = new BufferedReader(new FileReader(PATH)))
        {
            String s;
            s = br.readLine();
            while((s=br.readLine())!=null){
                csvArr = s.split(",");
                if(Integer.parseInt(csvArr[0]) != id ){
                    rewrite(s, PATH2);
                }
                else {
                    findID = 1;
                    Person updatePerson = createPerson.updatePerson(id);
                    updatePerson.setId(id);
                    String updateCsv = parseToStringCsv(updatePerson);
                    rewrite(updateCsv, PATH2);
                }
            }
            if (findID==0){
                System.out.println(MSG_PERSON_NOT_FOUND);
            }
            else System.out.println(MSG_PERSON_UPDATE);
        }
        catch(IOException e){
            System.out.println("my error js");
            e.printStackTrace();
        }
        csvFile.delete();
        boolean success = csvFile22.renameTo(csvFile);

        if (success){
            System.out.println(MSG_FILE_UPDATE);
        }
        else
        {
            System.out.println("Error file");
        }
    }

    @Override
    public void delete() {
        int id = validationService.validateID();
        int findID = 0;
        File csvFile2 = new File(PATH2);
        try (BufferedReader br = new BufferedReader(new FileReader(PATH))) {
            String s;
            String[] csvArr;
            s = br.readLine();
            while((s=br.readLine())!=null){
                csvArr = s.split(",");
                if(Integer.parseInt(csvArr[0]) != id ){
                    rewrite(s, PATH2);

                }else {
                    findID = 1;
                }
            }
            if (findID==0){
                System.out.println(MSG_PERSON_NOT_FOUND);
            }
            else System.out.println(MSG_PERSON_UPDATE);

        } catch (IOException e) {
            System.out.println("my error js");
            e.printStackTrace();
        }

        csvFile.delete();
        boolean success = csvFile2.renameTo(csvFile);

        if (success) {
            System.out.println("Person delete");
        } else {
            System.out.println("Error file");
        }
    }


    private void rewrite(String csvStr, String pathFileToWrite) {
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(pathFileToWrite, true))) {
            checkHeadLine(PATH2);
            bufferedWriter.write(csvStr + "\n");
            bufferedWriter.flush();//��������
        } catch (IOException e) {//������������ �����
            System.out.println("my error js");
            e.printStackTrace();
        }
    }


    private void checkHeadLine(String nameFile) {
        try (BufferedReader br = new BufferedReader(new FileReader(nameFile))) {//������ ���������
            String firstLine;
            firstLine = br.readLine();
            if (firstLine == null ) {
                createHeadLine(nameFile);
            }
        } catch (IOException e) {
            System.out.println("my error js read");
            e.printStackTrace();
        }
    }


    private void createHeadLine(String nameFile) {
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(nameFile, true))) {
            bufferedWriter.write(HEAD_CSV + "\n");
            bufferedWriter.flush();
        } catch (IOException e) {
            System.out.println("my error js");
            e.printStackTrace();
        }
    }
    private String parseToStringCsv(Person person) {
        return person.getId() +","+ person.getFname() + "," + person.getLname()  + "," + person.getAge() + "," + person.getCity() ;
    }

}

