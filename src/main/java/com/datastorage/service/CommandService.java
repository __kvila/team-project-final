package main.java.com.datastorage.service;

import java.util.Scanner;

import main.java.com.datastorage.cache.FactoryCache;


public class CommandService {

    private static final String MSG_ENTER_ID = "Enter ID: ";

    private static final String MSG_WELCOME = "Welcome to the Data Storage App!";
    //private static final String MSG_AVAILABLE_COMMANDS = "Select: choose file, help or exit.";
    private static final String MSG_INVALID_COMMAND = "Invalid command!";
    private static final String MSG_ENTER_EXTENSION = "Enter file extension";
    //private static final String MSG_SELECTED_EXTENSION = "You working with ";
    private static final String MSG_AVAILABLE_CRUD_COMMANDS = "Select: create, read, readAll, update, delete, switch, help or exit.";
    private static final String MSG_EXTENSIONS = "Select extension: .xml, .binary, .csv, .yaml, .json.";
    //private static final String MSG_CURRENT_EXTENSION = "You are currently working with: ";
    private static final String RESPONSE_HELP = "help";
    private static final String RESPONSE_EXIT = "exit";
    //private static final String RESPONSE_CHOOSE_FILE = "choose file";
    private static final String RESPONSE_CREATE = "create";
    private static final String RESPONSE_READ = "read";
    private static final String RESPONSE_READALL = "readAll";
    private static final String RESPONSE_UPDATE = "update";
    private static final String RESPONSE_DELETE = "delete";
    private static final String RESPONSE_SWITCH = "switch";
    //private static final String RESPONSE_NAME = "name";
    private static final String HELP = "Select extension(.xml, .binary, .csv, .yaml, .json.) for working with file.\n" +
            "Select command: \"create\", \"update\" or \"delete\" for create, update or delete person;\n " +
            "\"read\" for raed one person by ID, \"readAll\" for read all person\n" +
            "\"switch\" for change file for working, \"help\" for read the help, \"exit\" for exit the application.";

    private static final String EXIT = "Goodbye!";


    private Scanner scanner;
    private FactoryCache factoryCache;
    private IFilesFactory factory;

    private static String fileExtension;
    private static String userResponse;
    private static boolean flag = true;


    public CommandService(Scanner scanner, FactoryCache factoryCache) {
        this.scanner = scanner;
        this.factoryCache = factoryCache;
    }

    public String scanResponse() {
        return scanner.next();
    }

    public void startApp() {
        System.out.println(MSG_WELCOME);
        selectEnvironment();
        if (factory == null) {
            System.out.println(MSG_INVALID_COMMAND);
            return;
        }
        while (true) {
            System.out.println(MSG_AVAILABLE_CRUD_COMMANDS);
            String command = scanResponse();
            processCommand(command);
        }
    }

    private void processCommand(String command) {
        switch (command) {
            case RESPONSE_CREATE:
                factory.create();
                break;
            case RESPONSE_UPDATE:
                System.out.println(MSG_ENTER_ID);
                factory.update();
                break;
            case RESPONSE_DELETE:
                System.out.println(MSG_ENTER_ID);
                factory.delete();
                break;
            case RESPONSE_READ:
                System.out.println(MSG_ENTER_ID);
                factory.read();
                break;
            case RESPONSE_READALL:
                factory.readAll();
                break;
            case RESPONSE_SWITCH:
                selectEnvironment();
                break;
            case RESPONSE_EXIT:
                System.out.println(EXIT);
                System.exit(0);
                break;
            case RESPONSE_HELP:
                System.out.println(HELP);
                break;
            default:
                System.out.println(MSG_INVALID_COMMAND);
        }
    }

    private void selectEnvironment() {
        System.out.println(MSG_EXTENSIONS);
        System.out.println(MSG_ENTER_EXTENSION);
        String env = scanner.next();
        factory = factoryCache.getEnvironment(env);
    }
}
