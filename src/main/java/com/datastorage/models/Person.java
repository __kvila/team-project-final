package main.java.com.datastorage.models;

import main.java.com.datastorage.service.CommandService;

import java.io.Serializable;
import java.util.Scanner;

public class Person implements Serializable{
    private int id;
    private String fname;
    private String lname;
    private int age;
    private String city;



    public Person() { }

    @Override
    public String toString() {
        return  "ID: " + id +
                " || first name: " + fname  +
                " || last name: " + lname +
                " || age: " + age +
                " || city: " + city;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
