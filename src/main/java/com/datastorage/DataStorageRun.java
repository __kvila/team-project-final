package main.java.com.datastorage;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.google.gson.Gson;
import de.undercouch.bson4jackson.BsonFactory;
import main.java.com.datastorage.cache.FactoryCache;
import main.java.com.datastorage.service.CommandService;
import main.java.com.datastorage.service.IFilesFactory;
import main.java.com.datastorage.service.ValidationService;
import main.java.com.datastorage.service.impl.*;
import main.java.com.datastorage.utils.CreateFile;
import main.java.com.datastorage.utils.CreatePerson;
import main.java.com.datastorage.wrapper.WrapperFileJSON;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class DataStorageRun {
    public static void main(String [] args) {



        Scanner scanner = new Scanner(System.in);

        ValidationService validationService= new ValidationService(scanner);

        CreatePerson createPerson = new CreatePerson();
        CreateFile createFile = new CreateFile();

        File binaryFile = createFile.createFile("src/Person.bin");
        File csvFile = createFile.createFile("src/csvFile.csv");
        File jsonFile = createFile.createFile("src/personJSON.json");
        File xmlFile = createFile.createFile("src/xmlFile.xml");
        File yamlFile = createFile.createFile("src/yamlPerson.yaml");


        //����������
        Gson gson = new Gson();
        XmlMapper xmlMapper = new XmlMapper();
        ObjectMapper binaryMapper = new ObjectMapper(new BsonFactory());
        ObjectMapper yamlMapper = new ObjectMapper(new YAMLFactory());


        //Wrapper for JSON
        WrapperFileJSON wrapperFileJSON = new WrapperFileJSON();

        //������� ������� ��� ������
        BinaryService binaryService = new BinaryService(scanner, validationService, createPerson, binaryMapper, binaryFile);
        CSVService csvService = new CSVService(createPerson, scanner, validationService, csvFile);
        JSONService jsonService = new JSONService(gson, createPerson, scanner, validationService, wrapperFileJSON, jsonFile);
        XMLService xmlService = new XMLService(scanner, validationService, createPerson, xmlMapper, xmlFile);
        YAMLService yamlService = new YAMLService(scanner, validationService, createPerson, yamlMapper);

        //������� ������ ��������� �������(��� � �������� ���� ������� ��� ������).
        // ��������� ���� ������� ������ � ��� �������, � ������� ��� ����� ���������� � FactoryCache
        IFilesFactory[] filesFactory = new IFilesFactory[]{binaryService, csvService, jsonService, xmlService, yamlService};
        //������� FactoryCache - �����, ��� ���������� ����� ����� �� ������ �������� ������� filesFactory
        FactoryCache factoryCache = new FactoryCache(filesFactory);

        while (true) {
            new CommandService(scanner, factoryCache).startApp();
        }



    }
}
